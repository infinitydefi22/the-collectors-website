import Head from 'next/head'

function MainContainer ({ children, title = '' }) {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name='keywords' content='landing' />
        <meta charSet='utf-8' />
      </Head>
      <div>
        {children}
      </div>
    </>
  )
}

export default MainContainer
