import Image from 'next/image'

const StrdtLanding = () => {
    return (
        <div className='position-relative'>
           <div className='position-absolute d-flex justify-content-center w-100'>
            <div className='d-flex flex-column text-center w-50 mt-5'>
                <h1>StarDust STRDT</h1>
                <p>The StarDast STRDT is our support, we manage to extract it from
                space even by being inside the spaceship, but when we go out into
                space it takes more.
                </p>
            </div>
           </div>
           <div className='bgWrapMore'>
                <Image
                    src='/bg-StarDust-STRDT.png'
                    layout='fill'
                    // objectFit='cover'
                    quality={100}
                />
            </div>
            <div className='position-absolute imgStrdt'>
                <Image
                    src='/StarDust-STRDT.png'
                    height={150}
                    width={150}
                    quality={100}
                />
            </div>
            <div className='position-absolute w-50 textStrdt'>
                <p>The StarDust is token, each token<br/>
                   Is minted by using 0,001 USD and<br/>
                   Is used to buy rooms and special object<br/>
                   From the marketplace
                </p>
            </div>
        </div>
    )
}

export default StrdtLanding