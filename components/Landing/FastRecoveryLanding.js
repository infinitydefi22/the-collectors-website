import Image from 'next/image'

const FastRecoveryLanding = () => {
    return (
        <div className='d-flex align-items-center recWrap'>
            <div className='position-absolute imgRec'>
                <Image
                    src='/Fast-Recovery.png'
                    height={400}
                    width={400}
                />
            </div>
            <div className='position-absolute textRec w-50'>
                <h1>Fast Recovery</h1>
                <h2 className='text-success'>Only 150 on 10.000</h2>
                <p>The collector with this gift is reborn faster
                   catching up in half the time of his teammates
                   Normally the recovery time is 3 days
                   but for those like him 1 day is enough.
                </p>
            </div>
        </div>
    )
}

export default FastRecoveryLanding