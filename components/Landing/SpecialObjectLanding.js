import Image from 'next/image'

const SpecialObjectLanding = () => {
    return (
        <div className='d-flex flex-column bgSpecial'>
            <div className='d-flex flex-column justify-content-center text-center mt-3 mb-3'>
                <h1>Special object in the space</h1>
                <p>
                    When we in space, our productivity increases!<br/>
                    And in space we can also finnd many unique and<br/> 
                    very useful special objects for our work!
                </p>
            </div>
            <div className='row mb-5'>
                <div className='col-md-6 d-flex justify-content-center'>
                    <div className='d-flex flex-column specialBg m-3 text-center m-3'>
                        <div className='d-flex justify-content-center m-2'>
                        <Image 
                            src='/Space-Compressor.png'
                            height={100}
                            width={100}
                        />
                        </div>
                        <h2 className='m-2'>Space<br/> Compressor</h2>
                        <h3 className='text-success m-2'>Invulnerability<br/> For 10 days</h3>
                        <h2 className='m-2'>Max 50 units</h2>
                    </div>
                    <div className='d-flex flex-column specialBg m-3 text-center m-3'>
                        <div className='d-flex justify-content-center m-2'>
                        <Image 
                            src='/Space-Viewer.png'
                            height={100}
                            width={100}
                        />
                        </div>
                        <h2 className='m-2'>Space<br/> Viewer</h2>
                        <h3 className='text-success m-2'>Invulnerability<br/> For 5 days</h3>
                        <h2 className='m-2'>Max 100 units</h2>
                    </div>
                </div>
                <div className='col-md-6 d-flex justify-content-center'>
                    <div className='d-flex flex-column specialBg m-3 text-center m-3'>
                        <div className='d-flex justify-content-center m-2'>
                        <Image 
                            src='/Space-VR.png'
                            height={100}
                            width={100}
                        />
                        </div>
                        <h2 className='m-2'>Space<br/> VR</h2>
                        <h3 className='text-success m-2'>APY 300%<br/> For 10 days</h3>
                        <h2 className='m-2'>Max 10 units</h2>
                    </div>
                    <div className='d-flex flex-column specialBg m-3 text-center m-3'>
                        <div className='d-flex justify-content-center m-2'>
                        <Image 
                            src='/Space-Solver.png'
                            height={100}
                            width={100}
                        />
                        </div>
                        <h2 className='m-2'>Space<br/> Solver</h2>
                        <h3 className='text-success m-2'>APY 300%<br/> For 5 days</h3>
                        <h2 className='m-2'>Max 20 units</h2>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SpecialObjectLanding