import Image from 'next/image'

const ChambersLanding = () => {
    return (
        <div className='position-relative'>
           <div className='bgWrapMore'>
                <Image
                    src='/bg-The-Chambers.png'
                    layout='fill'
                    // objectFit='cover'
                    quality={100}
                />
            </div>
            <div className='position-absolute alienPower'>
                <Image
                    src='/The-Chambers.png'
                    height={300}
                    width={300}
                    quality={100}
                />
            </div>
            <div className='position-absolute textPower w-50'>
                <h1>The Chambers</h1>
                <p>One is the Resurrection Chamber and is
                    the Swap Chamber. The chamber
                    can be mint here.
                    <br/>
                    <br/>    
                    When you own a chamber you can lend it!
                    <br/>
                    <br/>
                    And if you don&apos;t want to build a new one,
                    or if you run out of stock, you can rent
                    someone else&apos;s
                </p>
            </div>
        </div>
    )
}

export default ChambersLanding