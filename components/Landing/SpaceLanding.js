import Image from 'next/image'

const SpaceLanding = () => {
    return (
        <div className='position-relative'>
           <div className='bgWrapStar'>
                <Image
                    src='/bg-The-Space.png'
                    layout='fill'
                    objectFit='cover'
                    quality={100}
                />
            </div>
            <div className='position-absolute imgSpace'>
                <Image
                    src='/The-Space-Planet.png'
                    height={250}
                    width={250}
                    quality={100}
                />
            </div>
            <div className='position-absolute textSpace w-50'>
                <h1>The Space</h1>
                <p>When we are in space, our productivity increases!<br/>
                   And in space we can also find many unique and<br/> 
                   very useful special objects for our work!
                </p>
            </div>
        </div>
    )
}

export default SpaceLanding