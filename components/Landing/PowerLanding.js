import Image from 'next/image'

const PowerLanding = () => {
    return (
        <div className='position-relative'>
           <div className='bgWrapMore'>
                <Image
                    src='/bgg-all-powers-super-nova.png'
                    layout='fill'
                    // objectFit='cover'
                    quality={100}
                />
            </div>
            <div className='position-absolute alienPower'>
                <Image
                    src='/all-powers-super-nova.png'
                    height={300}
                    width={300}
                />
            </div>
            <div className='position-absolute textPower w-50'>
                <h1>All the Powers</h1>
                <p>Each one of us is unique and special.
                   but there is someones whois more special than others.
                   <br/>
                   <br/>
                   Few of us possess regenerative powers, whether for
                   ourselves. for other or for both of us, some
                   regenerative much faster than others, and some
                   are completely immune to attacks from humans.
                </p>
            </div>
        </div>
    )
}

export default PowerLanding