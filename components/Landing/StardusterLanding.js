import Image from 'next/image'

const StardusterLanding = () => {
    return (
        <div className='d-flex align-items-center invWrap'>
            <div className='position-absolute textInv text-end w-50'>
                <h1>Starduster</h1>
                <h2 className='text-success'>Only 100 on 10.000</h2>
                <p>The collector with this power can collect
                   More StarDust when in the mother ship
                   Normally each collector is able to collect
                   10% per year but those kind will collect 15% in the mother ship
                </p>
            </div>
            <div className='position-absolute imgInv'>
                <Image
                    src='/Starduster.png'
                    height={400}
                    width={400}
                />
            </div>
        </div>
    )
}

export default StardusterLanding