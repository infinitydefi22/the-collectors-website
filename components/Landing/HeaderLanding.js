import Image from 'next/image'

const HeaderLanding = () => {
    return (
        <div className='conainer-landing position-relative'>
            <div className='bgWrap'>
                <Image
                    src='/bg-header.png'
                    layout='fill'
                    // objectFit='cover'
                    objectPosition='center'
                    quality={100}
                />
            </div>
           <div className='logoWrap p-4'>
               <Image 
                    src='/logo.png'
                    height={50} 
                    width={420} 
                    // layout='fill'
                />
            </div>
            <div className='twitWrap m-4'>
                <a href={process.env.NEXT_PUBLIC_TWITTER} target='_blank' rel='noreferrer'>
                    <Image 
                        src='/twitter-white.png'
                        height={50} 
                        width={50} 
                    />
                </a>
            </div>
            <div className='discWrap m-4'>
                <a href={process.env.NEXT_PUBLIC_DISCORD} target='_blank' rel='noreferrer'>
                    <Image 
                        src='/discord-white.png'
                        height={50} 
                        width={50} 
                    />
                </a>
            </div>
           <div className='img-motherWrap'>
               <Image 
                    src='/mother-ship.png' 
                    height={450} 
                    width={750}
                    // layout='fill' 
                    
                />
            </div>
           <div className='card border-0 text-landing-header'>
               <div className='card-body'>
                <h1>We are dispersed in space</h1>
                <br />
                <p>After the humans destroyed our planet we escaped on our mother spaceship.<br/>
                    And from there we survive. Most of us sleep in a long time.<br/>
                    Other collect resources for the community.
                    <br/>
                    <br/>
                    Coming out the ship, we can find many more resources in space,
                    but space is dangerous for us. Humans are always on the prowl
                    and kill us when they find us in space.
                </p>
               </div>
           </div>
           <div className='alien-conteiner d-flex'>
              <div className='img-alienGreen'><Image src='/alien-die.png' height={200} width={100}  /></div>
              <div className='img-alienOrange'><Image src='/alien-alive.png' height={250} width={200} /></div>
           </div>
        </div>
    )
}

export default HeaderLanding