import Image from 'next/image'
import MyLink from '../UI/button/MyLink'

const FooterLanding = () => {
    return (
        <div className='position-relative'>
           <div className='bgWrapMore'>
                <Image
                    src='/footer.png'
                    layout='fill'
                    // objectFit='cover'
                    quality={100}
                />
            </div>
            <div className='position-absolute d-flex justify-content-center w-100 joinDiscord'>
                <div className='position-relative'>
                    <Image
                        className='joinDiscordImg'
                        src='/bg-Join-Discord-space.png'
                        height={200}
                        width={900}
                        quality={100}
                    />
                    <div className='position-absolute text-center d-flex justify-content-center w-100 textJoinDiscord'>
                        <h1>Join now our <br/> Discord Server</h1>
                    </div>
                    <div className='d-flex justify-content-center mt-5'>
                        <MyLink href={process.env.NEXT_PUBLIC_DISCORD} color='btn-success' image='/discord-white.png'>Join Discord</MyLink>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FooterLanding