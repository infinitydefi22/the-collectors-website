import Image from 'next/image'

const ResChamberLanding = () => {
    return (
        <div className='d-flex align-items-center resWrap'>
            <div className='textRes w-50 position-absolute text-end'>
                <h1>The Resurrection Chamber</h1>
                <p>The Resurrection Chamber Brings them
                   back from the dead,
                   When they are in the skeletal condition
                   they cannot work to extract StarDust.
                   For this you need a 6-hour treatment
                   in the Resurrection Chamber
                   to get back to work.
                </p>
            </div>
            <div className='imgRes position-absolute'>
                <Image
                    src='/The-Resurrection-Chamber.png'
                    height={250}
                    width={250}
                />
            </div>
        </div>
    )
}

export default ResChamberLanding