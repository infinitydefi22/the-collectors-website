import Image from 'next/image'

const SpaceshipLanding = () => {
    return (
        <div className='position-relative'>
           <div className='bgWrapMore'>
                <Image
                    src='/bg-Human-Spaceship.png'
                    layout='fill'
                    // objectFit='cover'
                    quality={100}
                />
            </div>
            <div className='position-absolute alienPower'>
                <Image
                    src='/Human-Spaceship.png'
                    height={300}
                    width={300}
                    quality={100}
                />
            </div>
            <div className='position-absolute textPower w-50'>
                <h1>Human Spaceship</h1>
                <p>The Human Spaceship every 6 hours makes a laser
                   shot in space killing a variable number of Collectors.
                   The number increases the more collectors there are
                   in space based on the total number of collectors
                   awakened from cryogenic sleep.
                   <br/>
                   <br/>
                   The Human Spaceship can kill only collectors
                   That are in the space.
                </p>
            </div>
        </div>
    )
}

export default SpaceshipLanding