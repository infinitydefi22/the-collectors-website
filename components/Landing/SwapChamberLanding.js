import Image from 'next/image'

const SwapChamberLanding = () => {
    return (
        <div className='d-flex align-items-center swapWrap'>
            <div className='position-absolute imgSwap'>
                <Image
                    src='/The-Swap-Chamber.png'
                    height={250}
                    width={250}
                />
            </div>
            <div className='position-absolute textSwap w-50'>
                <h1>The Swap Chamber</h1>
                <p>The Swap Chamber swap the ability of one
                    Collector to another one.
                    <br/>
                    <br/>
                    But sometimes did not work as expected
                    And create anew kind of power.
                </p>
            </div>
        </div>
    )
}

export default SwapChamberLanding