import Image from 'next/image'
import MyLink from '../UI/button/MyLink'

const CollectLanding = () => {
    return (
        <div className='d-flex align-items-center collectWrap'>
            <div className='w-50 position-absolute textCollect'>
                <h1>We are here to collect</h1>
                <p>We live in the space, collecting stardast to survive!
                   Once pur world was destroyed by the humans. And now, the
                   Only safe place is our mothership.
                   <br/>
                   <br/>
                   Some of sometimes go out in the space to collect more
                   But the humans are alwais looking for us in the space
                   And they shoot us with powerful laser cannon.
                   <br/>
                   <br/>
                   Lucky for us our tecnology can resurrect us!
                   Also very few of us are invulnerable, and some of us have healing powers.
                </p>
                <div className='mt-3 d-flex flex-column align-items-center w-100'>
                    <div>
                        <MyLink href={process.env.NEXT_PUBLIC_DISCORD} color='btn-warning' image='/discord-white.png'>Join Discord</MyLink>
                        <div className='d-flex justify-content-center text-center w-75 mt-3'>
                            <p>10.000 of us still sleep in the cryogenic sleep</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className='position-absolute imgCollect'>
                <Image
                    src='/rocket.png'
                    height={400}
                    width={400}
                />
            </div>
        </div>
    )
}

export default CollectLanding