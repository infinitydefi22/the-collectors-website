import Image from 'next/image'

const HeroLanding = () => {
    return (
        <div className='position-relative'>
           <div className='bgWrapMore'>
                <Image
                    src='/bg-die.png'
                    layout='fill'
                    // objectFit='cover'
                    quality={100}
                />
            </div>
            <div className='position-absolute alienHero'>
                <Image
                    src='/alien-die.png'
                    height={300}
                    width={200}
                />
            </div>
            <div className='position-absolute textHero w-50'>
                <h1>To Die</h1>
                <p>Death is never the end for us, our tecnology allows us to come back to life,
                   but it is always a waste of time and energy.
                </p>
            </div>
        </div>
    )
}

export default HeroLanding