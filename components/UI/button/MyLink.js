import Image from 'next/image'
const MyButton = ({ image, children, color, href }) => {
    return (
        <div className=''>
            <a href={href}  target='_blank' className={`w-shadow position-relative btn ${color}`} rel='noreferrer'>{children}
                {image
                ?<div className='icon-button'>
                    <Image 
                    src={image}
                    height={50}
                    width={50} 
                    />
                </div>
                : null
                }               
            </a>
        </div>
    )
}

export default MyButton