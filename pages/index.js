import MainContainer from '../components/MainContainer'

import HeaderLanding from '../components/Landing/HeaderLanding'
import HeroLanding from '../components/Landing/HeroLanding'
import CollectLanding from '../components/Landing/CollectLanding'
import PowerLanding from '../components/Landing/PowerLanding'
import InvulnerabilityLanding from '../components/Landing/InvulnerabilityLanding'
import FastRecoveryLanding from '../components/Landing/FastRecoveryLanding'
import StardusterLanding from '../components/Landing/StardusterLanding'
import SpaceLanding from '../components/Landing/SpaceLanding'
import SpecialObjectLanding from '../components/Landing/SpecialObjectLanding'
import SpaceshipLanding from '../components/Landing/SpaceshipLanding'
import StrdtLanding from '../components/Landing/StrdtLanding'
import ChambersLanding from '../components/Landing/ChambersLanding'
import SwapChamberLanding from '../components/Landing/SwapChamberLanding'
import ResChamberLanding from '../components/Landing/ResChamberLanding'
import FooterLanding from '../components/Landing/FooterLanding'

function Home () {
  return (
    <MainContainer title='Landing'>
      <div className='landingWrap'>
        <HeaderLanding />
        <HeroLanding />
        <CollectLanding />
        <PowerLanding />
        <InvulnerabilityLanding />
        <FastRecoveryLanding />
        <StardusterLanding />
        <SpaceLanding />
        <SpecialObjectLanding />
        <SpaceshipLanding />
        <StrdtLanding />
        <ChambersLanding />
        <SwapChamberLanding />
        <ResChamberLanding />
        <FooterLanding />
      </div>
    </MainContainer>
  )
}
export default Home
